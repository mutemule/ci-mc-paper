# Container Image for a Paper Minecraft server

This creates a container image for a Paper Minecraft server. I think. I
hope? This whole world of Minecraft is confusing.

# Objective

Most publicly-built container images tend to be bloated and insecure,
depending on services like S6 which pull in large base systems, and run
as root with the full Linux capabilities.

This image is small and lean, and is designed to be run requiring no
special privileges at all.

# Requirements

A single volume that can be mounted into both this container, as well
as the dnscrypt-proxy container.

# Using

```sh
docker  run -d \
    --name mc-paper \
    --init \
    --volume papermc-data:/data \
    --volume papermc-config:/config \
    --tmpfs /tmp \
    --network games \
    --publish 25565:25565/tcp \
    --read-only \
    --cap-drop all \
    --security-opt no-new-privileges \
    --pids-limit 50 \
    registry.gitlab.com/mutemule/ci-mc-paper/main:latest > "/var/run/mc-paper.cid"
```
