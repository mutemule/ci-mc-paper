FROM docker.io/library/alpine:latest AS builder

RUN apk update && \
    apk add curl jq

RUN paper_version="$(curl -sL https://papermc.io/api/v2/projects/paper | jq -r '.versions[-1]')" && \
    paper_build="$(curl -sL https://papermc.io/api/v2/projects/paper/versions/${paper_version} | jq -r '.builds[-1]')" && \
    install -m 0755 -o root -g root -d /opt/minecraft && \
    curl -sL "https://papermc.io/api/v2/projects/paper/versions/${paper_version}/builds/${paper_build}/downloads/paper-${paper_version}-${paper_build}.jar" -o /opt/minecraft/paper.jar


FROM docker.io/library/openjdk:17-slim
    
LABEL maintainer="Damian Gerow <dwg@flargle.io>"
LABEL name="minecraft-paper"

ARG MEMORY="8G"
ENV JAVA_MEMORY="$MEMORY"

# Some wonky JVM tuning nonsense
# ENV JAVA_OPTIONS=" -Dusing.aikars.flags=mcflags.emc.gs -Dcom.mojang.eula.agree=true -Djava.awt.headless=true -server -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1"
ENV JAVA_OPTIONS="-Djava.awt.headless=true -server -XX:+UseG1GC -XX:MaxGCPauseMillis=200"

# Based on Oracle Linux, which ... I can't find the package manager for?
RUN apt -y update && \
    apt -y upgrade && \
    apt -y clean && \
    rm -rf /var/lib/lists/* /tmp/* /var/cache/apt/*

COPY --from=builder /opt/ /opt/

RUN useradd -c "Minecraft" -M -d /home/minecraft -s /sbin/nologin -U minecraft && \
    install -m 0755 -o minecraft -g minecraft -d /data

EXPOSE 25565/tcp

VOLUME [ "/data" ]

USER minecraft
WORKDIR /data

ENTRYPOINT [ "java", "-server", "-Djava.awt.headless=true", "-Xms4G", "-Xmx8G", "-jar", "/opt/minecraft/paper.jar" ]
CMD [ "--nogui" ]
